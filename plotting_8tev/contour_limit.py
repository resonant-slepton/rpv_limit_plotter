#!/usr/bin/env python
from __future__ import division
import sys
import logging as log
import numpy as np
import ROOT as r
import optparse
from collections import OrderedDict
from rootpy.io import root_open
from rootpy.plotting import root2matplotlib as rplt
import matplotlib as mpl
import matplotlib.tri as mtri
from matplotlib import rc
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy.interpolate import griddata,interpolate
import scipy


class axis_spec():
    def __init__(self, ax_min, ax_max, ax_label = ""):
        self.limits = [ax_min,ax_max]
        self.ax_min = ax_min
        self.ax_max = ax_max
        self.label = ax_label

class format_clabel(float):
     def __repr__(self):
         str = '%.3f' % (self.__float__(),)
         if str[-1]=='0':
             return '%.2f' % self.__float__()
         else:
             return '%.3f' % self.__float__()

def read_xs(m0,m12):
    xs_file = open("input/xsec_nlo_muon.txt", 'r')
    for line in xs_file:
        line_split = line.split()
        #xs is in 3rd column of file
        if len(line_split) > 3:
            if (float(line_split[0])==m0) and (float(line_split[1])==m12):
                return float(line_split[2])
        else:
            log.error("Error while parsing the following line of the config file (not enough coulumns.",line_split)
            sys.exit()
    log.debug("Could not read xs for the following point (m0=%f,m12=%f)"%(m0,m12))
    return 0.

def read_line_from_file(file, columns = [0,1]):
    try:
        f = open(file, 'rU')
    except:
        log.error("Could not read file:%s"%(file))
        sys.exit()
    content = OrderedDict()
    for i in range(0,len(columns)):
        col = columns[i]
        content[str(col)] = []
    for line in f:
        line = line.split()
        for i in range(0,len(columns)):
            col = columns[i]
            content[str(col)].append(float(line[col]))
    f.close()
    return tuple(content.values())

def long_edges(x, y, triangles, radio=120):
    out = []
    for points in triangles:
        #print points
        a,b,c = points
        d0 = np.sqrt( (x[a] - x[b]) **2 + (y[a] - y[b])**2 )
        d1 = np.sqrt( (x[b] - x[c]) **2 + (y[b] - y[c])**2 )
        d2 = np.sqrt( (x[c] - x[a]) **2 + (y[c] - y[a])**2 )
        max_edge = max([d0, d1, d2])
        #print points, max_edge
        if max_edge > radio:
            out.append(True)
        else:
            out.append(False)
    return out



def draw_lp_limit_from_table(input_file, is_ele = True, check_interp = False, show_grids = False, save_plot = False):
    # read limithist from root file
    x, y, z = read_line_from_file(input_file, columns = [2,3,7])
    xi, yi = np.meshgrid(np.arange(0,2310,10),np.arange(0,1210,10))

    # set axes range and labels
    if is_ele:
        z_label = r'$\lambda^{\prime}_{111}$'
        x_label = r'$\rm{m_{\tilde{e}} [GeV]}$'
    else:
        z_label = r'$\lambda^{\prime}_{211}$'
        x_label = r'$\rm{m_{\tilde{\mu}} [GeV]}$'

    y_label = r'$\rm{m_{\tilde{\chi}^{0}} [GeV]}$'
    x_spec = axis_spec(0,2205,x_label)
    y_spec = axis_spec(0,1115,y_label)
    z_spec = axis_spec(0.0005,0.1,z_label)

    triang = mtri.Triangulation(x, y)
    mask = long_edges(x,y, triang.triangles)
    triang.set_mask(mask)
    interp_cubic_geom = mtri.CubicTriInterpolator(triang, z, kind='geom')
    zi = interp_cubic_geom(xi, yi)

    font = {'size': 25}
    mpl.rc('font', **font)
    mpl.rcParams['lines.linewidth'] = 2
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(15, 15))

    cont = [0.003,0.01,0.02,0.03]
    if is_ele:
        draw_interp_subplot(ax, zi, x_spec, y_spec, z_spec, xi = xi, yi = yi, zi = zi, show_grids = show_grids, cont = cont, triang = triang)
    else:
        interior = np.sqrt(((xi-785)**2) + (yi-100)**2) < 10
        draw_interp_subplot(ax, zi, x_spec, y_spec, z_spec, xi = xi, yi = yi, zi = zi, show_grids = show_grids, cont = cont, triang = triang, masked_region = interior)

    ax.annotate(r'CMS', xy=(20, -20), xycoords='axes points', horizontalalignment='left', verticalalignment='top', fontsize=35, fontweight='semibold')
    #ax.annotate(r'Work in progress', xy=(20, -55), xycoords='axes points', horizontalalignment='left', verticalalignment='top', fontsize=25, style='italic')
    ax.annotate(r'Preliminary', xy=(20, -55), xycoords='axes points', horizontalalignment='left', verticalalignment='top', fontsize=25, style='italic')
    ax.annotate(r'observed %s limit'%z_label, xy=(20, -95), xycoords='axes points', horizontalalignment='left', verticalalignment='top', fontsize=25)
    ax.annotate(r'$19.7\ \rm{fb^{-1}} (8\ TeV)$', xy=(1.00, 1.0), xycoords='axes fraction', horizontalalignment='right', verticalalignment='bottom', fontsize=25)
    if is_ele:
        cms8tev_limit = ax.plot([-1],[-1], c='black', linestyle = '-', label = 'CMS 8 TeV $\lambda_{111}^{\prime}$'+'\n'+r'$\tan(\beta)$ = 20,'+'\n'+r'$\mu$ > 0, $A_{0}$ = 0', linewidth=2)
    else:
        cms8tev_limit = ax.plot([-1],[-1], c='black', linestyle = '-', label = 'CMS 8 TeV $\lambda_{211}^{\prime}$'+'\n'+r'$\tan(\beta)$ = 20,'+'\n'+r'$\mu$ > 0, $A_{0}$ = 0', linewidth=2)
    ax.legend(loc=(0.001,0.70),prop={'size':18},frameon = False)

    plt.tight_layout()
    if save_plot:
        if is_ele:
            plt.savefig('Ele_sparticle_lambda.pdf',bbox_inches='tight')
            plt.savefig('Ele_sparticle_lambda.png',bbox_inches='tight')
        else:
            plt.savefig('Muon_sparticle_lambda.pdf',bbox_inches='tight')
            plt.savefig('Muon_sparticle_lambda.png',bbox_inches='tight')
    else:
        plt.show()

def draw_lp_limit(input_file, h_name = "lambda_new", is_ele = True, check_interp = False, show_grids = False, save_plot = False):
    # read limithist from root file
    try:
        f_input = root_open(input_file)
    except:
        log.error("Error while reading the following file: %s"%input_file)
        sys.exit()
    try:
        h_limit = f_input.Get(h_name)
    except:
        log.error("Error while reading hist %s from file %s"%(h_name,f_input))
        sys.exit()

    # read values from limithist and transfrom to lp limit
    x, y, z = [], [], []
    for i in range(1,h_limit.GetNbinsX()+1):
        for j in range(1,h_limit.GetNbinsY()+1):
            limit = h_limit.GetBinContent(i,j)
            if abs(limit) > 0.1:
                h_limit.SetBinContent(i,j,0)
            if abs(limit) > 0.00001 and abs(limit) <= 0.1:
                m0 = h_limit.GetXaxis().GetBinCenter(i)
                m12 = h_limit.GetYaxis().GetBinCenter(j)
                xs = read_xs(m0,m12)
                limit = h_limit.GetBinContent(i,j)
                if xs > 0 and limit > 0:
                    #limit_lp = ((limit/xs)**0.5)*0.01
                    limit_lp = limit
                    h_limit.SetBinContent(i,j,limit_lp)
                    x.append(m0)
                    y.append(m12)
                    z.append(limit_lp)
                    log.debug("Read point: \t bin_x %d, bin_y %d, m0 %f, m12 %f, xs %f, xs-limit %f, lp-limit %f"%(i,j,m0,m12,xs,limit,limit_lp))
                else:
                    h_limit.SetBinContent(i,j,0)

    interpolate_borders(h_limit,x,y,z)

    # create x,y lists for points used in interpolation
    #xi, yi = np.meshgrid(np.linspace(0,1550,155),np.linspace(0,2550,255))
    xi, yi = np.meshgrid(np.arange(0,1551,10),np.arange(0,2551,10))

    # set axes range and labels
    if is_ele:
        z_label = r'$\lambda^{\prime}_{111}$'
    else:
        z_label = r'$\lambda^{\prime}_{211}$'
    #x_label = r'$\rm{m_0 [GeV]}$'
    x_label = r'$\rm{m_0 [GeV]}$'
    y_label = r'$\rm{m_{1/2} [GeV]}$'
    x_spec = axis_spec(0,1550,x_label)
    y_spec = axis_spec(0,2550,y_label)
    z_spec = axis_spec(0.0005,0.1,z_label)

    # check different interpolation methods if option is set
    if check_interp:
        check_interpolation(x, y, z, xi, yi, h_limit, x_spec, y_spec, z_spec, show_grids = show_grids)

    font = {'size': 25}
    mpl.rc('font', **font)
    mpl.rcParams['lines.linewidth'] = 2
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(15, 15))

    triang = mtri.Triangulation(x, y)
    interp_cubic_geom = mtri.CubicTriInterpolator(triang, z, kind='geom')
    zi = interp_cubic_geom(xi, yi)
    mask_regions(h_limit,xi,yi,zi)

    cont = [0.003,0.01,0.02,0.03]
    draw_interp_subplot(ax, h_limit, x_spec, y_spec, z_spec, xi = xi, yi = yi, zi = zi, show_grids = show_grids, cont = cont)
    ax.annotate(r'CMS', xy=(20, -20), xycoords='axes points', horizontalalignment='left', verticalalignment='top', fontsize=35, fontweight='semibold')
    #ax.annotate(r'Work in progress', xy=(20, -55), xycoords='axes points', horizontalalignment='left', verticalalignment='top', fontsize=25, style='italic')
    ax.annotate(r'Preliminary', xy=(20, -55), xycoords='axes points', horizontalalignment='left', verticalalignment='top', fontsize=25, style='italic')
    ax.annotate(r'observed %s limit'%z_label, xy=(20, -95), xycoords='axes points', horizontalalignment='left', verticalalignment='top', fontsize=25)
    ax.annotate(r'$19.7\ \rm{fb^{-1}} (8\ TeV)$', xy=(1.00, 1.0), xycoords='axes fraction', horizontalalignment='right', verticalalignment='bottom', fontsize=25)
    if not is_ele:
        d0_x, d0_y = read_line_from_file("input/d0_limit.txt")
        d0_x, d0_y = mask_regions_line(h_limit,d0_x,d0_y)
        d0_limit = ax.plot(d0_x,d0_y, c='r', linestyle = '-.', label=r'D0 $\lambda_{211}^{\prime}$ < 0.05'+'\n'+r'$\tan(\beta)$ = 5,'+'\n'+'$\mu$ < 0, $A_{0}$ = 0', linewidth=2)

        cms7tev_x, cms7tev_y = read_line_from_file("input/cms7tev_limit.txt")
        cms7tev_x, cms7tev_y = mask_regions_line(h_limit,cms7tev_x,cms7tev_y)
        cms7tev_limit = ax.plot(cms7tev_x,cms7tev_y, c='red', linestyle = '-', label = 'CMS 7 TeV $\lambda_{211}^{\prime}$ < 0.01'+'\n'+r'$\tan(\beta)$ = 20,'+'\n'+r'$\mu$ > 0, $A_{0}$ = 0', linewidth=2)

        ax.plot((1300,1300), (0,1000), 'k-', c = 'grey')
        ax.plot((0,1300), (1000,1000), 'k-', c = 'grey')

        ax.plot((500,500), (0,600), 'k-', c = 'grey')
        ax.plot((0,500), (600,600), 'k-', c = 'grey')

        ax.annotate('CMS 7 TeV', xy=(10,1020), xycoords='data', fontsize = 20 , color = 'grey')
        ax.annotate('scan', xy=(10,945), xycoords='data', fontsize = 20 , color = 'grey')
        ax.annotate('D0', xy=(10,620), xycoords='data', fontsize = 20 , color = 'grey')
        ax.annotate('scan', xy=(10,545), xycoords='data', fontsize = 20 , color = 'grey')

        cms8tev_limit = ax.plot([-1],[-1], c='black', linestyle = '-', label = 'CMS 8 TeV $\lambda_{211}^{\prime}$'+'\n'+r'$\tan(\beta)$ = 20,'+'\n'+r'$\mu$ > 0, $A_{0}$ = 0', linewidth=2)
        ax.legend(loc=(0.001,0.60),prop={'size':18},frameon = False)
    else:
        cms8tev_limit = ax.plot([-1],[-1], c='black', linestyle = '-', label = 'CMS 8 TeV $\lambda_{111}^{\prime}$'+'\n'+r'$\tan(\beta)$ = 20,'+'\n'+r'$\mu$ > 0, $A_{0}$ = 0', linewidth=2)
        ax.legend(loc=(0.001,0.70),prop={'size':18},frameon = False)

    plt.tight_layout()
    if save_plot:
        if is_ele:
            fig.savefig('lp111_limit.pdf',bbox_inches='tight')
            fig.savefig('lp111_limit.png',bbox_inches='tight')
        else:
            plt.savefig('lp211_limit.pdf',bbox_inches='tight')
            plt.savefig('lp211_limit.png',bbox_inches='tight')
    else:
        plt.show()


def check_neighbours(h,i,j):
    #return true if one of the neighouring bins is empty / false otherwise
    #lists = [(i,j-1),(i+1,j),(i,j+1),(i-1,j)]
    lists = [(i+1,j),(i,j+1)]
    empty = []
    for x,y in lists:
        if i < 1 or j < 1:
            continue
        if h.GetBinContent(x,y) == 0:
            empty.append([x,y])
    return empty


def interpolate_borders(h_limit, x, y, z):
    for i in range(1,h_limit.GetNbinsX()+1):
        for j in range(1,h_limit.GetNbinsY()+1):
            limit_lp = h_limit.GetBinContent(i,j)
            if abs(limit_lp) == 0:
                continue
            empty_bins = check_neighbours(h_limit,i,j)
            if len(empty_bins) > 0:
                m0_ij = h_limit.GetXaxis().GetBinCenter(i)
                m12_ij = h_limit.GetYaxis().GetBinCenter(j)
                if not (m0_ij == 1500 or m12_ij == 2500):
                    continue
                for k,l in empty_bins:
                    m0_kl = h_limit.GetXaxis().GetBinCenter(k)
                    m12_kl = h_limit.GetYaxis().GetBinCenter(l)
                    if i == k:
                        dist = h_limit.GetXaxis().GetBinWidth(i)
                        if j > l:
                            extrap_limit = ((h_limit.GetBinContent(i,j+1)-h_limit.GetBinContent(i,j))/dist)*(dist/2)+limit_lp
                            y.append(m12_ij-dist/2)
                        else:
                            extrap_limit = ((h_limit.GetBinContent(i,j)-h_limit.GetBinContent(i,j-1))/dist)*(dist/2)+limit_lp
                            y.append(m12_ij+dist/2)
                        x.append(m0_ij)
                    elif j == l:
                        dist = h_limit.GetYaxis().GetBinWidth(j)
                        if i > k:
                            extrap_limit = ((h_limit.GetBinContent(i+1,j)-h_limit.GetBinContent(i,j))/dist)*(dist/2)+limit_lp
                            x.append(m0_ij-dist/2)
                        else:
                            extrap_limit = ((h_limit.GetBinContent(i,j)-h_limit.GetBinContent(i-1,j))/dist)*(dist/2)+limit_lp
                            x.append(m0_ij+dist/2)
                        y.append(m12_ij)
                    z.append(extrap_limit)


def mask_regions(hist,xi,yi,zi):
    for (x,y), value in np.ndenumerate(zi):
        x_tmp = xi[x,y]
        y_tmp = yi[x,y]
        i = hist.GetXaxis().FindBin(x_tmp)
        j = hist.GetYaxis().FindBin(y_tmp)
        if hist.GetBinContent(i,j) == 0:
            zi[x,y] = np.ma.masked


def mask_regions_line(hist,xi,yi):
        xi_new, yi_new = [], []
        for x_tmp,y_tmp in zip(xi,yi):
            i = hist.GetXaxis().FindBin(float(x_tmp))
            j = hist.GetYaxis().FindBin(float(y_tmp))
            if not hist.GetBinContent(i,j) == 0:
                xi_new.append(x_tmp)
                yi_new.append(y_tmp)
        return xi_new,yi_new


def draw_interp_subplot(ax, limit, x_spec, y_spec, z_spec, triang = None, xi = None, yi = None, zi = None, cont = None, title = "", show_grids = False, masked_region = None):
    if isinstance(limit,r.TH2):
        img = rplt.imshow(limit, ax, norm=mpl.colors.LogNorm(),  vmin = z_spec.ax_min , vmax = z_spec.ax_max)
    elif isinstance(limit,np.ma.core.MaskedArray):
        dx = (xi[2][2]-xi[1][1])
        xi_n = xi - dx/2.
        dy = (yi[2][2]-yi[1][1])
        yi_n = yi - dy/2.
        img = ax.pcolor(xi_n,yi_n,limit, vmin = z_spec.ax_min, vmax = z_spec.ax_max, norm=mpl.colors.LogNorm())
    else:
        log.error("Argument 'limit' has wrong type: %s"%type(limit))
        sys.exit()
    ax.set_xlim(x_spec.limits)
    ax.set_xlabel(x_spec.label, fontsize = 45)
    ax.set_ylim(y_spec.limits)
    ax.set_ylabel(y_spec.label, fontsize = 45)
    ax.set_title(title)
    if not xi==None and not yi==None and not zi==None and cont == None:
        c = ax.contour(xi, yi, zi, colors='black')
        ax.clabel(c, inline=1, fontsize=15)
    if not xi==None and not yi==None and not zi==None:
        if masked_region == None:
            c = ax.contour(xi, yi, zi, levels = cont, colors='black', extrapolate = True, interp = True)
        else:
            zi[masked_region] = np.ma.masked
            c = ax.contour(xi, yi, zi, levels = cont, colors='black', extrapolate = True, interp = True)
        c.levels = [format_clabel(val) for val in c.levels ]
        # uncomment to print out contour lines
        #p = c.collections[1].get_paths()[0]
        #v = p.vertices
        #x = v[:,0]
        #y = v[:,1]
        #for i in range(len(x)):
            #print x[i],y[i]
        #print x
        #print y
        fmt = r'%r'
        ax.clabel(c, fontsize=18, fmt = fmt)
    if show_grids:
        if triang:
            ax.triplot(triang, 'ko-')
        if (not xi == None):
            ax.plot(xi_n, yi_n, 'k-', alpha=0.5)
        if (not yi == None):
            ax.plot(xi_n.T, yi_n.T, 'k-', alpha=0.5)
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(img, cax=cax)
    cbar.set_label(z_spec.label,fontsize=45)


def check_interpolation(x, y, z, xi, yi, h, x_spec, y_spec, z_spec, show_grids = False):

    triang = mtri.Triangulation(x, y)

    interp_lin = mtri.LinearTriInterpolator(triang, z)
    zi_lin = interp_lin(xi, yi)

    interp_cubic_geom = mtri.CubicTriInterpolator(triang, z, kind='geom')
    zi_cubic_geom = interp_cubic_geom(xi, yi)

    interp_cubic_min_E = mtri.CubicTriInterpolator(triang, z, kind='min_E')
    zi_cubic_min_E = interp_cubic_min_E(xi, yi)

    mask_regions(h,xi,yi,zi_lin)
    mask_regions(h,xi,yi,zi_cubic_geom)
    mask_regions(h,xi,yi,zi_cubic_min_E)

    # first figure
    font = {'size': 15}
    mpl.rc('font', **font)
    fig1, ((ax11, ax12), (ax13, ax14)) = plt.subplots(nrows=2, ncols=2, figsize=(25, 25))

    cont = [0.01,0.02,0.03]

    # first subplot: triangulation of limit plot and original limit plot
    if show_grids:
        title = 'original limit with triangular grid'
        draw_interp_subplot(ax11, h, x_spec, y_spec, z_spec, triang = triang, title = title, show_grids = show_grids, cont = cont)
    else:
        title = 'original limitplot'
        draw_interp_subplot(ax11, h, x_spec, y_spec, z_spec, title = title, show_grids = show_grids, cont = cont)
    print 'Triangular grid done'

    # second subplot: original limit with contours from linear interpolation and interpolation grid
    if show_grids:
        title = 'original limit with interp. grid and contours (linear interp.)'
    else:
        title = 'original limit with contours (linear interp.)'
    draw_interp_subplot(ax12, h, x_spec, y_spec, z_spec, xi = xi, yi = yi, zi = zi_lin, title = title, show_grids = show_grids, cont = cont)
    print 'Linear interpolation done'

    ## third subplot: original limit with contours from cubic-geom. interpolation and interpolation grid
    if show_grids:
        title = 'original limit with interp. grid and contours (cubic-geom. interp.)'
    else:
        title = 'original limit with contours (cubic-geom. interp.)'
    draw_interp_subplot(ax13, h, x_spec, y_spec, z_spec, xi = xi, yi = yi, zi = zi_cubic_geom, title = title, show_grids = show_grids, cont = cont)
    print 'Cubic interpolation (geom) done'

    ## fourth subplot: original limit with contours from cubic-min_E interpolation and interpolation grid
    if show_grids:
        title = 'original limit with interp. grid and contours (cubic-minE interp.)'
    else:
        title = 'original limit with contours (cubic-min_E interp.)'
    draw_interp_subplot(ax14, h, x_spec, y_spec, z_spec, xi = xi, yi = yi, zi = zi_cubic_min_E, title = title, show_grids = show_grids, cont = cont)
    print 'Cubic interpolation (minE) done'


    # second figure
    fig2, ((ax21, ax22), (ax23, ax24)) = plt.subplots(nrows=2, ncols=2, figsize=(25, 25))
    cont = [0.01,0.02,0.03]

    # first subplot: triangulation of limit plot and interp. limit plot
    if show_grids:
        title = 'interp. limit with triangular grid'
        draw_interp_subplot(ax21, h, x_spec, y_spec, z_spec, triang = triang, title = title, show_grids = show_grids, cont = cont)
    else:
        title = 'interp. limitplot'
        draw_interp_subplot(ax21, h, x_spec, y_spec, z_spec, title = title, show_grids = show_grids, cont = cont)
    print 'Triangular grid done'

    # second subplot: interp. limit with contours from linear interpolation and interpolation grid
    if show_grids:
        title = 'interp. limit with interp. grid and contours (linear interp.)'
    else:
        title = 'interp. limit with contours (linear interp.)'
    draw_interp_subplot(ax22, zi_lin, x_spec, y_spec, z_spec, xi = xi, yi = yi, zi = zi_lin, title = title, show_grids = show_grids, cont = cont, triang = triang)
    print 'Linear interpolation done'

    ## third subplot: interp. limit with contours from cubic-geom. interpolation and interpolation grid
    if show_grids:
        title = 'interp. limit with interp. grid and contours (cubic-geom. interp.)'
    else:
        title = 'interp. limit with contours (cubic-geom. interp.)'
    draw_interp_subplot(ax23, zi_cubic_geom, x_spec, y_spec, z_spec, xi = xi, yi = yi, zi = zi_cubic_geom, title = title, show_grids = show_grids, cont = cont, triang = triang)
    print 'Cubic interpolation (geom) done'

    ## fourth subplot: interp. limit with contours from cubic-min_E interpolation and interpolation grid
    if show_grids:
        title = 'interp. limit with interp. grid and contours (cubic-minE interp.)'
    else:
        title = 'interp. limit with contours (cubic-min_E interp.)'
    draw_interp_subplot(ax24, zi_cubic_min_E, x_spec, y_spec, z_spec, xi = xi, yi = yi, zi = zi_cubic_min_E, title = title, show_grids = show_grids, cont = cont, triang = triang)
    print 'Cubic interpolation (minE) done'

    plt.show()


def main():
    usage = "usage: %prog [options]"
    parser = optparse.OptionParser(usage)

    parser.add_option("--check_interp",
              action="store_true", dest="check_interp", default=False,
              help="Produce extra plots to show the interpolation.")
    parser.add_option("-s","--save_plots",
              action="store_true", dest="save_plots", default=False,
              help="Save plots as pdf files.")
    parser.add_option("--show_grids",
              action="store_true", dest="show_grids", default=False,
              help="Show grids used for interpolation")
    elefile_default = "input/LimitCMSSM.root"
    parser.add_option("--ele_file", dest="ele_file", default=elefile_default,
              help="Root file with electron limit. Default:"+elefile_default)
    muofile_default = "input/LimitCMSSM_Muon_interpolated.root"
    parser.add_option("--muo_file", dest="muo_file", default=muofile_default,
              help="Root file with muon limit. Default:"+muofile_default)
    (opt, args) = parser.parse_args()


    log.basicConfig(level=log.WARNING)

    draw_lp_limit(opt.ele_file, check_interp = opt.check_interp, show_grids = opt.show_grids, save_plot = opt.save_plots)
    draw_lp_limit(opt.muo_file, check_interp = opt.check_interp, show_grids = opt.show_grids, is_ele=False, save_plot = opt.save_plots)

    draw_lp_limit_from_table("input/elelimit_particle.txt", check_interp = opt.check_interp, show_grids = opt.show_grids, save_plot = opt.save_plots)
    draw_lp_limit_from_table("input/muolimit_particle.txt", check_interp = opt.check_interp, show_grids = opt.show_grids, is_ele=False, save_plot = opt.save_plots)

    sys.exit()


# Standard boilerplate to call the main() function to begin
# the program.
if __name__ == '__main__':
    main()

