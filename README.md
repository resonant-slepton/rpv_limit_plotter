# RPVSleptonLimits

# Requirements
Some of the functions may need a newer matplotlib and numpy version. To install these, follow the instructions below.

```bash
# download and install numpy v1.6.2
wget http://sourceforge.net/projects/numpy/files/NumPy/1.6.2/numpy-1.6.2.tar.gz
tar xzf numpy-1.6.2.tar.gz
cd numpy-1.6.2
python setup.py install --user
cd ..
# download and install matplotlib v1.4.3
wget https://downloads.sourceforge.net/project/matplotlib/matplotlib/matplotlib-1.4.3/matplotlib-1.4.3.tar.gz
tar xzf matplotlib-1.4.3.tar.gz
cd cd matplotlib-1.4.3
python setup.py install --user
cd ..
```
