#!/usr/bin/env python
from __future__ import division
import sys
import logging as log
import numpy as np
import ROOT as r
import argparse
from collections import OrderedDict
from rootpy.io import root_open
from rootpy.plotting import root2matplotlib as rplt
import matplotlib as mpl
import matplotlib.tri as mtri
from matplotlib import rc
from matplotlib import pyplot as plt
from matplotlib.patches import Patch
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.legend import Legend
from scipy.interpolate import griddata,interpolate
import matplotlib.lines as mlines
import scipy


class axis_spec():
    def __init__(self, ax_min, ax_max, ax_label = ""):
        self.limits = [ax_min,ax_max]
        self.ax_min = ax_min
        self.ax_max = ax_max
        self.label = ax_label

class format_clabel(float):
     def __repr__(self):
         str = '%.3f' % (self.__float__(),)
         if str[-1]=='0':
             return '%.2f' % self.__float__()
         else:
             return '%.3f' % self.__float__()

class format_clabelMH(float):
     def __repr__(self):
         if self.__float__() == 124.:
            return r'$m_{\mathrm{H}}$'+' = {0:.0f} GeV'.format(self.__float__())
         else:
            return r'{0:.0f} GeV'.format(self.__float__())

def read_line_from_file(file, columns = [0,1]):
    try:
        f = open(file, 'rU')
    except:
        log.error("Could not read file:%s"%(file))
        sys.exit()
    content = OrderedDict()
    for i in range(0,len(columns)):
        col = columns[i]
        content[str(col)] = []
    for line in f:
        line = line.split()
        if line[0][0] == '#':
            continue
        for i in range(0,len(columns)):
            col = columns[i]
            content[str(col)].append(float(line[col]))
    f.close()
    return tuple(content.values())

def mask_regions_line(xi,yi):
        xi_new, yi_new = [], []
        for x_tmp,y_tmp in zip(xi,yi):
            if y_tmp > 225:
                xi_new.append(x_tmp)
                yi_new.append(y_tmp)
        return xi_new,yi_new

def long_edges(x, y, triangles, radio=1500):
    out = []
    for points in triangles:
        #print points
        a,b,c = points
        d0 = np.sqrt( (x[a] - x[b]) **2 + (y[a] - y[b])**2 )
        d1 = np.sqrt( (x[b] - x[c]) **2 + (y[b] - y[c])**2 )
        d2 = np.sqrt( (x[c] - x[a]) **2 + (y[c] - y[a])**2 )
        max_edge = max([d0, d1, d2])
        if max_edge > radio:
            out.append(True)
        else:
            out.append(False)
    return out

def drawmHContourLine(ax, value, args):
    x, y, z = read_line_from_file(args.mHFile, columns = [0, 1, 2])
    if value == 126:
        xi, yi = np.meshgrid(np.arange(950,2050,1),np.arange(2900,3200,1))
    elif value == 125:
        xi, yi = np.meshgrid(np.arange(750,2550,1),np.arange(2200,2600,1))
    elif value == 124:
        xi, yi = np.meshgrid(np.arange(600,2700,1),np.arange(1800,2200,1))
    else:
        xi, yi = np.meshgrid(np.arange(0,3050,50),np.arange(0,3200,50))
    triang = mtri.Triangulation(x, y)
    interp_cubic_geom = mtri.CubicTriInterpolator(triang, z, kind='geom')
    zi = interp_cubic_geom(xi, yi)
    c = ax.contour(xi, yi, zi, levels = [value], colors='black', linestyles = 'dashed', extrapolate = True, interp = True)
    c.levels = [format_clabelMH(val) for val in c.levels ]
    fmt = r'%r'
    if value == 126:
        manual_location = [(1250,3000)]
    elif value == 125:
        manual_location = [(1000,3000)]
    elif value == 124:
        manual_location = [(1100,3000)]
    else:
        manual_location = [(1300,3000)]
    ax.clabel(c, fontsize=22, fmt=fmt, manual=manual_location, inline=1, inline_spacing = 25)

def draw_lp_limit_from_table(column, args):
    x, y, z = read_line_from_file(args.inputFile, columns = [0, 1, column])
    xi, yi = np.meshgrid(np.arange(0,3250,50),np.arange(0,3400,50))

    # set axes range and labels
    x_label = r'm$_{0}$ (GeV)'
    y_label = r'm$_{1/2}$ (GeV)'
    z_label = r'$\lambda^{\prime}_{211}$'
    x_spec = axis_spec(0,3050,x_label)
    y_spec = axis_spec(0,3450,y_label)
    z_spec = axis_spec(0.001,0.1,z_label)

    triang = mtri.Triangulation(x, y)
    mask = long_edges(x,y, triang.triangles)
    triang.set_mask(mask)
    interp_cubic_geom = mtri.CubicTriInterpolator(triang, z, kind='geom')
    zi = interp_cubic_geom(xi, yi)

    # matlplotlib settings
    font = {'size': 25}
    mpl.rc('font', **font)
    mpl.rcParams['lines.linewidth'] = 2
    mpl.rcParams['font.family'] = 'sans-serif'
    mpl.rcParams['mathtext.fontset'] = 'custom'
    mpl.rcParams['mathtext.rm'] = 'Bitstream Vera Sans'
    mpl.rcParams['mathtext.it'] = 'Bitstream Vera Sans:italic'
    mpl.rcParams['xtick.labelsize'] = 35
    mpl.rcParams['ytick.labelsize'] = 35
    mpl.rcParams['mathtext.bf'] = 'Bitstream Vera Sans:bold'
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(15, 15))

    cont = [0.00001]
    interior = np.sqrt(((xi-890)**2) + (yi-2000)**2) < 50
    interior2 =  np.sqrt(((xi-950)**2) + (yi-3100)**2) < 50
    interior += interior2
    # interior = None
    draw_interp_subplot(ax, zi, x_spec, y_spec, z_spec, xi = xi, yi = yi, zi = zi, showGrids = args.showGrids, cont = cont, triang = triang, masked_region = interior)

    ax.annotate(r'35.9 fb$^{-1}$ (13 TeV)', xy=(1.00, 1.0), xycoords='axes fraction', horizontalalignment='right', verticalalignment='bottom', fontsize=25)
    ax.annotate(r'$\tan\beta=20$', xy=(18, -30), xycoords='axes points', horizontalalignment='left', verticalalignment='top', fontsize=35)
    ax.annotate(r'$A_{0}$=0$\,$GeV', xy=(18, -75), xycoords='axes points', horizontalalignment='left', verticalalignment='top', fontsize=35)
    ax.annotate(r'$\operatorname{sgn}(\mu)$=+', xy=(18, -117), xycoords='axes points', horizontalalignment='left', verticalalignment='top', fontsize=35)

    if column == 7:
        legendLabel = 'Observed limit'
    elif column == 4:
        legendLabel = 'Expected limit'
    ax.annotate(legendLabel, xy=(400, -30), xycoords='axes points', horizontalalignment='left', verticalalignment='top', fontsize=35)

    plt.tight_layout()
    if args.show:
        plt.show()
    else:
        basename = 'lp211_limit_thesis_comparion'
        if column == 7:
            basename += '_obs'
        elif column == 4:
            basename += '_exp'
        else:
            log.error('Unknown colum: {:d}'.format(column))
            sys.exit(1)
        if args.showGrids:
            basename += '_grids'
        if args.tag is not None:
	    basename += '_' + args.tag
        plt.savefig(basename+'.pdf',bbox_inches='tight')
        plt.savefig(basename+'.png',bbox_inches='tight')

def draw_interp_subplot(ax, limit, x_spec, y_spec, z_spec, triang = None, xi = None, yi = None, zi = None, cont = None, title = "", showGrids = False, masked_region = None):
    if isinstance(limit,r.TH2):
        img = rplt.imshow(limit, ax, norm=mpl.colors.LogNorm(),  vmin = z_spec.ax_min , vmax = z_spec.ax_max)
    elif isinstance(limit,np.ma.core.MaskedArray):
        dx = (xi[2][2]-xi[1][1])
        xi_n = xi - dx/2.
        dy = (yi[2][2]-yi[1][1])
        yi_n = yi - dy/2.
        img = ax.pcolor(xi_n,yi_n,limit, vmin = z_spec.ax_min, vmax = z_spec.ax_max, norm=mpl.colors.LogNorm(), rasterized = True)
    else:
        log.error("Argument 'limit' has wrong type: %s"%type(limit))
        sys.exit()
    ax.tick_params(direction='in', length=18, width=2)
    ax.xaxis.get_major_ticks()[0].tick1On=False
    ax.xaxis.get_major_ticks()[0].tick2On=False
    ax.yaxis.get_major_ticks()[0].tick1On=False
    ax.yaxis.get_major_ticks()[0].tick2On=False
    ax.tick_params(direction='in', length=9, width=2, which = 'minor')
    ax.set_xticks([0,500,1000,1500,2000,2500,3000])
    ax.set_xticks([100, 200, 300, 400, 600, 700, 800, 900, 1100, 1200, 1300, 1400, 1600, 1700, 1800, 1900, 2100, 2200, 2300, 2400, 2600, 2700, 2800, 2900], minor = True)
    ax.set_yticks([0,500,1000,1500,2000,2500,3000])
    ax.set_yticks([i*100 for i in range(1,44)], minor = True)
    ax.set_xlim(x_spec.limits)
    ax.set_xlabel(x_spec.label, fontsize = 45)
    ax.set_ylim(y_spec.limits)
    ax.set_ylabel(y_spec.label, fontsize = 45, family='sans-serif')
    ax.set_title(title)
    if not xi==None and not yi==None and not zi==None and cont == None:
        c = ax.contour(xi, yi, zi, colors='black')
        ax.clabel(c, inline=1, fontsize=15)
    if not xi==None and not yi==None and not zi==None:
        if masked_region == None:
            c = ax.contour(xi, yi, zi, levels = cont, colors='black', extrapolate = True, interp = True, antialiased=True)
        else:
            zi[masked_region] = np.ma.masked
            c = ax.contour(xi, yi, zi, levels = cont, colors='black', extrapolate = True, interp = True, antialiasesd = True)
        c.levels = [format_clabel(val) for val in c.levels]
        # p = c.collections[0].get_paths()[1]
        # v = p.vertices
        # x = v[:,0]
        # y = v[:,1]
        # for i in range(len(x)):
        #     print x[i],y[i]

        fmt = r'%r'
        ax.clabel(c, fontsize=25, fmt = fmt)
    if showGrids:
        if triang:
            ax.triplot(triang, 'ko-')
        if (not xi == None):
            ax.plot(xi_n, yi_n, 'k-', alpha=0.5)
        if (not yi == None):
            ax.plot(xi_n.T, yi_n.T, 'k-', alpha=0.5)
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.10)
    tickList = [0.001*i for i in range(1,10)]
    tickList += [0.01*i for i in range(1,10)]
    cbar = plt.colorbar(img, cax=cax, ticks = [0.001,0.01,0.1])
    cbar.solids.set_rasterized(True)
    cbar.ax.minorticks_on()
    cbar.ax.tick_params(which = 'minor', length=12, width = 1)
    cbar.ax.tick_params(length = 20, width = 1, color ='black')
    cbar.ax.yaxis.set_ticks(img.norm(tickList), minor=True)
    cbar.set_label(z_spec.label,fontsize=45)

    #d0_x, d0_y = read_line_from_file("input/d0_limit.txt")
    #d0_limit = ax.plot(d0_x,d0_y, c='r', linestyle = '-.', label=r'D0 $\lambda_{211}^{\prime}$ < 0.05'+'\n'+r'$\tan(\beta)$ = 5,'+'\n'+'$\mu$ < 0, $A_{0}$ = 0', linewidth=2)

    # cms7tev_x, cms7tev_y = read_line_from_file("input/cms7tev_limit.txt")
    # cms7tev_x, cms7tev_y = mask_regions_line(h_limit,cms7tev_x,cms7tev_y)
    # cms7tev_limit = ax.plot(cms7tev_x,cms7tev_y, c='green', linestyle = '-', label = 'CMS 7 TeV $\lambda_{211}^{\prime}$ < 0.01'+'\n'+r'$\tan(\beta)$ = 20,'+'\n'+r'$\mu$ > 0, $A_{0}$ = 0', linewidth=2)

    # cms8tev_x_0p003, cms8tev_y_0p003 = read_line_from_file("input/cms8tev_limit_0p003.txt")
    # cms8tev_x_0p003, cms8tev_y_0p003 = mask_regions_line(cms8tev_x,cms8tev_y)
    # cms8tev_limit_0p003 = ax.plot(cms8tev_x_0p003,cms8tev_y_0p003, c='red', linestyle = '--', label = '$\lambda_{211}^{\prime}$ < 0.003', linewidth=2)

    cms8tev_x_0p01, cms8tev_y_0p01 = read_line_from_file("input/cms8tev_limit_0p01.txt")
    cms8tev_x_0p01, cms8tev_y_0p01 = mask_regions_line(cms8tev_x_0p01,cms8tev_y_0p01)
    cms8tev_limit_0p01 = ax.plot(cms8tev_x_0p01,cms8tev_y_0p01, c='red', linestyle = '-', label = '$\lambda_{211}^{\prime}$ < 0.01', linewidth=2)

    cms8tev_x_0p02_pt1, cms8tev_y_0p02_pt1 = read_line_from_file("input/cms8tev_limit_0p02_pt1.txt")
    cms8tev_x_0p02_pt1, cms8tev_y_0p02_pt1 = mask_regions_line(cms8tev_x_0p02_pt1,cms8tev_y_0p02_pt1)
    cms8tev_limit_0p02_pt1 = ax.plot(cms8tev_x_0p02_pt1,cms8tev_y_0p02_pt1, c='red', linestyle = '--', label =  '$\lambda_{211}^{\prime}$ < 0.02', linewidth=2)

    cms8tev_x_0p02_pt2, cms8tev_y_0p02_pt2 = read_line_from_file("input/cms8tev_limit_0p02_pt2.txt")
    cms8tev_x_0p02_pt2, cms8tev_y_0p02_pt2 = mask_regions_line(cms8tev_x_0p02_pt2,cms8tev_y_0p02_pt2)
    cms8tev_limit = ax.plot(cms8tev_x_0p02_pt2,cms8tev_y_0p02_pt2, c='red', linestyle = '--', linewidth=2)

    # cms13tev_x_0p003, cms13tev_y_0p003 = read_line_from_file("input/cms13tev_limit_0p003.txt")
    # cms13tev_x_0p003, cms13tev_y_0p003 = mask_regions_line(cms13tev_x_0p003,cms13tev_y_0p003)
    # cms13tev_limit_0p003 = ax.plot(cms13tev_x_0p003,cms13tev_y_0p003, c='black', linestyle = '--', linewidth=2)

    cms13tev_x_0p01, cms13tev_y_0p01 = read_line_from_file("input/cms13tev_limit_0p01.txt")
    cms13tev_x_0p01, cms13tev_y_0p01 = mask_regions_line(cms13tev_x_0p01,cms13tev_y_0p01)
    cms13tev_limit_0p01 = ax.plot(cms13tev_x_0p01,cms13tev_y_0p01, c='black', linestyle = '-',  linewidth=2)

    cms13tev_x_0p02, cms13tev_y_0p02 = read_line_from_file("input/cms13tev_limit_0p02.txt")
    cms13tev_x_0p02, cms13tev_y_0p02 = mask_regions_line(cms13tev_x_0p02,cms13tev_y_0p02)
    cms13tev_limit_0p02 = ax.plot(cms13tev_x_0p02,cms13tev_y_0p02, c='black', linestyle = '--', linewidth=2)

    ax.plot((1550,1550), (0,2550), 'k-', c = 'dimgrey', linewidth=3)
    ax.plot((0,1550), (2550,2550), 'k-', c = 'dimgrey', linewidth=3)

    ax.annotate('CMS 8 TeV', xy=(68,2570), xycoords='data', fontsize = 29 , color = 'dimgrey')
    ax.annotate('scan', xy=(77,2465), xycoords='data', fontsize = 29 , color = 'dimgrey')

    ax.legend(loc=(0.65,0.025),prop={'size':31},frameon = False, handlelength=1.5,handletextpad=0.05, labelspacing=0.01,columnspacing=0.01, ncol = 1, borderpad=0.05)

    red_line = mlines.Line2D([], [], color='red', label='red stars')
    black_line = mlines.Line2D([], [], color='black', label='red stars')
    leg = Legend(ax, [black_line, red_line], ['13 TeV', '8 TeV'], loc=(0.07,0.025),prop={'size':31},frameon = False, handlelength=1.5,handletextpad=0.05, labelspacing=0.01,columnspacing=0.01, ncol = 2, borderpad=0.05)
    ax.add_artist(leg);



def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('inputFile',
                        help='Text file with lp limit values.')
    parser.add_argument("--checkInterp",
                        action="store_true",
                        default=False,
                        help="Produce extra plots to show the interpolation.")
    parser.add_argument("--show",
                        action="store_true",
                        default=False,
                        help="Show plots.")
    parser.add_argument("--showGrids",
                        action="store_true",
                        default=False,
                        help="Show grids used for interpolation")
    parser.add_argument("--mHFile",
			type = str,
                        default='higgs_masses.txt',
                        help="File with higgs masses")
    parser.add_argument('-t', '--tag',
			type=str,
                        default=None,
                        help='Tag added to plot names')
    args = parser.parse_args()
    log.basicConfig(level=log.WARNING)
    # observed limit
    draw_lp_limit_from_table(7, args)
    # expected limit
    # draw_lp_limit_from_table(4, args)


# Standard boilerplate to call the main() function to begin
# the program.
if __name__ == '__main__':
    main()

